from django import forms

from .models import *


class CommentsPostForm(forms.ModelForm):
    class Meta:
        model = CommentsPost
        fields = ('text_comment',)