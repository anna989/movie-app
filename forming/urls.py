from django.urls import path
from . import views

app_name = 'forming'
urlpatterns = [
    path('',  views.page_home, name='page_home'),
    path('<int:pk>/', views.PostView.as_view(), name='post_view'),
]