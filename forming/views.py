from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import FormView, DetailView

from .forms import CommentsPostForm
from .models import *


def page_home(request):
    posts = Post.objects.all()
    return render(request, 'forming/page-home.html', {'posts': posts})


class PostView(DetailView, FormView):
    model = Post
    template_name = 'forming/post-view.html'
    context_object_name = 'post'
    form_class = CommentsPostForm

    def get_success_url(self, **kwargs):
        return reverse_lazy('forming:post_view', kwargs={'pk': self.get_object().id})

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.post = self.get_object()
        self.object.user = self.request.user
        self.object.save()
        return super().form_valid(form)



