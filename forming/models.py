from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=60)
    description = models.TextField(max_length=255)

    def __str__(self):
        return self.title


class CommentsPost(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey('Post', on_delete=models.CASCADE)
    text_comment = models.TextField(max_length=255)

    def __str__(self):
        return self.user.name
