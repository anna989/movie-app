from smtplib import SMTPDataError
from urllib import request

from django.contrib.auth import get_user_model, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.generic import ListView, TemplateView

from .forms import RegisterUserForm, LoginUserForm, ProfileUpdateForm, UserUpdateForm

from django.contrib import messages

from .models import Profile
from .tokens import account_activation_token
from django.template.loader import render_to_string
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_str
from django.core.mail import send_mail
from django.conf import settings
from django.utils.html import format_html


def activate(request, uidb64, token):
    User = get_user_model()
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except:
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        messages.success(request, "Спасибо за подтверждение по электронной почте. Теперь вы успешно авторизованы.")
        Profile.objects.create(user_id=request.user.id)
        #return redirect('movie:home')
    else:
        messages.error(request, format_html("Ссылка активации недействительна! Если Вы не успели активировать учетную запись по ссылке, воспользуйтесь формой восстановления доступа. Для этого нажмите на <a href='/reactivation.html' class='alert-link'>ссылку</a>."))

    return redirect('movie:home')


def activateEmail(request, user, to_email):
    subject = "Активируйте свою учетную запись пользователя."
    context = {
        'user': user.username,
        'domain': get_current_site(request).domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'token': account_activation_token.make_token(user),
        "protocol": 'https' if request.is_secure() else 'http',
    }
    text = render_to_string("user/template_activate_account.html", context)
    html = render_to_string("user/template_activate_account.html", context)

    send_mail(subject, message=text, html_message=html, from_email=settings.EMAIL_HOST_USER, recipient_list=[to_email], fail_silently=False)


def register(request):
    if request.method == "POST":
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active=False
            user.save()
            try:
                activateEmail(request, user, form.cleaned_data.get('email'))
                messages.success(request, 'На указанный адрес электронной почты придет письмо активации учетной записи. Для завершения регистрации перейдите по ссылке в письме.')
                return redirect('movie:home')
            except SMTPDataError:
                form.add_error('email', 'Ошибка электронной почты')
            except Exception as error:
                HttpResponse(f'Ошибка: {error}')

        else:
            for error in list(form.errors.values()):
                messages.error(request, error)

    else:
        form = RegisterUserForm()

    return render(
        request=request,
        template_name="user/register.html",
        context={"form": form}
        )


class LoginUser(LoginView):
    form_class = LoginUserForm
    template_name = 'user/login.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Авторизация'
        return context

    def get_success_url(self):
        next_url = self.request.GET.get('next')
        return next_url if next_url else reverse_lazy('movie:home')


def logout_user(request):
    logout(request)
    return redirect('user:login')


class ProfileView(LoginRequiredMixin, ListView):
    model = Profile
    template_name = 'user/profile.html'
    context_object_name = 'profile'
    login_url = 'user:login'

    def get_queryset(self):
        return Profile.objects.get(user_id=self.request.user)


class ProfileEditView(LoginRequiredMixin, TemplateView):
    user_form = UserUpdateForm
    profile_form = ProfileUpdateForm
    template_name = 'user/profile-edit.html'
    login_url = 'user:login'

    def post(self, request):
        post_date = request.POST or None
        file_data = request.FILES or None
        user_form = UserUpdateForm(post_date, instance=request.user)
        profile_form = ProfileUpdateForm(post_date, file_data, instance=request.user.profile)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            return redirect('user:profile')

        context = self.get_context_data(user_form=user_form, profile_form=profile_form)

        return self.render_to_response(context)

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)