let dropdownToggle = document.querySelector('[data-content="dropdown-toggle"]')
let dropdownMenu = document.querySelector('[data-content="dropdown-menu"]')

if (dropdownToggle && dropdownMenu) {
    dropdownToggle.addEventListener('click', function(event) {
         event.preventDefault();
         dropdownMenu.classList.toggle('active');
    });

    document.addEventListener('click',e => {
        if (e.target != dropdownToggle && dropdownMenu.classList.contains('active')) {
            dropdownMenu.classList.remove('active')
        }
    })

};

function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
};