from django.contrib.auth.models import User
from django.db import models


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    photo = models.ImageField(default='default_user.png', upload_to='profile')

    def __str__(self):
        return f'{self.user}'
