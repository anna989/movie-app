from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, SetPasswordForm
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.views import PasswordResetConfirmView

from .models import Profile


class RegisterUserForm(UserCreationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Логин'}))
    email = forms.CharField(label='E-mail', widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'E-mail'}))
    password1 = forms.CharField(label='Пароль', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Пароль'}))
    password2 = forms.CharField(label='Повтор пароля', widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': 'Повтор пароля'}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean(self):
        cleaned_data = self.cleaned_data
        msg_email = "Эта почта уже зарегестрированна"
        msg_user = "Пользователь с таким именем уже существует"
        if User.objects.filter(email=cleaned_data.get('email')).exists():
            self._errors['email'] = self.error_class([msg_email])
        elif User.objects.filter(username=cleaned_data.get('username')).exists():
            self._errors['username'] = self.error_class([msg_user])

            del cleaned_data['email']
            del cleaned_data['username']
        return cleaned_data


class LoginUserForm(AuthenticationForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Логин'}))
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Пароль'}))


class UserUpdateForm(forms.ModelForm):
    username = forms.CharField(label='Логин', widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Логин'}))
    email = forms.CharField(label='E-mail', widget=forms.EmailInput(
        attrs={'class': 'form-control', 'placeholder': 'E-mail'}))

    class Meta:
        model = User
        fields = ('username', 'email')


class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('photo',)

