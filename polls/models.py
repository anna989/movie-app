from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200, verbose_name='Вопрос')
    pub_date = models.DateTimeField(verbose_name='Дата публикации')
    is_publish = models.BooleanField(verbose_name='Опубликовано', default=True)

    def __str__(self):
        return f'{self.question_text} {self.pub_date.strftime("%m-%d-%Y %H:%M")} {self.is_publish}'


class Choice(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    choice_text = models.TextField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return f'{self.choice_text} {self.votes}'
