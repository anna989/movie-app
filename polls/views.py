from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView, DetailView

from .models import *


class HomeView(ListView):
    template_name = 'polls/home.html'
    context_object_name = 'question_list'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now(), is_publish=True).order_by('-pub_date')


class DetailedView(DetailView):
    model = Question
    template_name = 'polls/detail.html'
    context_object_name = 'question'

    def get_queryset(self):
        return Question.objects.filter(pub_date__lte=timezone.now(), is_publish=True)


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': 'Выберите ответ'
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))


class ResultView(DetailedView):
    model = Question
    template_name = 'polls/result.html'
    context_object_name = 'question'
