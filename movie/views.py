from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView, DetailView
from django.views.generic.base import View
from .forms import ReviewForm, RatingForm

from .models import *


class MoviesListView(ListView):
    """ Список Фильмов """
    model = Movie
    template_name = 'movie/movies-list.html'
    context_object_name = 'movies'
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Movilab'
        context['movies'] = Movie.objects.filter(is_publish=True)[:40]  # Сделать Рандомный фильтр
        return context


class MoviesFullCategoriesView(ListView):
    """ Категории фильмов """
    model = Category
    template_name = 'movie/full-categories.html'
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Коллекции'
        context['categories'] = Category.objects.filter(is_publish=True)
        return context


class MoviesCategoryView(MoviesListView):
    """ Список фильмов в соответствии с категорией """
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['movies'] = Movie.objects.filter(categories__pk=self.kwargs['pk'], is_publish=True)
        return context


class MoviesGenreView(MoviesListView):
    """ Список Фильмов в соответствии с жанром """
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['movies'] = Movie.objects.filter(genres__pk=self.kwargs['pk'], is_publish=True)
        return context


class MoviesActorView(MoviesListView):
    """ Список Фильмов. Фильтр по Актерам """
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['movies'] = Movie.objects.filter(actors__pk=self.kwargs['pk'], is_publish=True)
        return context


class MoviesCountryView(MoviesListView):
    """ Список Фильмов. Фильтр по Странам """
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['movies'] = Movie.objects.filter(countries__pk=self.kwargs['pk'], is_publish=True)
        return context


class MovieDetailView(DetailView):
    """ Фильм """
    model = Movie
    template_name = 'movie/movie.html'
    context_object_name = 'movie'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['actors'] = context['movie'].actors.all()[:14]
        context['countries'] = context['movie'].countries.all()
        context['genres'] = context['movie'].genres.all()
        context['directors'] = context['movie'].directors.all()
        context['fav'] = context['movie'].favorites.filter(pk=self.request.user.id)
        context['reviews'] = context['movie'].reviews_movie.all()  # filter(status=True)
        context['star_form'] = RatingForm()
        star_checked = RatingView.objects.filter(movie_id=self.kwargs['pk'], user_id=self.request.user.id)
        if star_checked.exists():
            context['star_checked'] = star_checked[0]
        return context


class HomeView(ListView):
    model = Movie
    template_name = 'movie/index.html'
    
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        cats = Category.objects.filter(is_publish=True)
        context['title'] = 'Movilab'
        context['movies'] = {k: Movie.objects.filter(categories=k) for k in cats}
        context['genres'] = Genre.objects.filter(is_publish=True)
        
        return context


def search(request):
    msg = " ".join(request.GET['q'].split())
    movies = Movie.objects.filter(title__iregex=rf'^{msg}|\s{msg}')
    
    context = {
        'movies': movies,
    }
    if movies and len(msg) > 1:
        return render(request, 'movie/search.html', context=context)
    else:
        return render(request, 'movie/page-error.html', context=context)


def favorite_list(request):
    if request.user.is_authenticated:
        new = Movie.objects.filter(favorites=request.user)
        return render(request, 'movie/favorites.html', {'new': new})
    else:
        return  render(request, 'movie/favorites.html', {})


@login_required
def favorite_add(request, pk):
    movie = get_object_or_404(Movie, id=pk)
    if movie.favorites.filter(id=request.user.id).exists():
        movie.favorites.remove(request.user)
    else:
        movie.favorites.add(request.user)
    return HttpResponseRedirect(request.META['HTTP_REFERER'])


class AddReviewView(View):
    
    def post(self, request, pk):
        form = ReviewForm(request.POST)
        movie = Movie.objects.get(id=pk)
        if form.is_valid():
            form = form.save(commit=False)
            form.movie = movie
            form.user_id = request.user.id
            form.save()
        return redirect('movie:movie', movie.pk)


class AddRatingView(View):
    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            RatingView.objects.update_or_create(
                movie_id=int(request.POST.get("movie")),
                defaults={'star': int(request.POST.get("radio"))},
                user_id=request.user.id,
            )

            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)


