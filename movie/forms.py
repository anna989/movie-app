from django import forms
from .models import Reviews, RatingView


class ReviewForm(forms.ModelForm):
    text_review = forms.CharField(widget=forms.Textarea(
        attrs={
            'placeholder': 'Оставьте отзыв',
            'class': 'form-input'
        }))

    class Meta:
        model = Reviews
        fields = ('text_review',)


class RatingForm(forms.ModelForm):
    choices = [num for num in range(1, 11)]
    star = forms.ChoiceField(widget=forms.RadioSelect(), choices=choices, required=False)

    class Meta:
        model = RatingView
        fields = ('star',)

