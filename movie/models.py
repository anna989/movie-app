from django.contrib.auth.models import User
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.db import models
from django.urls import reverse


class Movie(models.Model):
    """ Фильм """
    QUALITY = [
        ('HD', 'HD'),
    ]
    TYPE = [
        ('FILM', 'FILM'),
    ]
    title = models.CharField(max_length=100, verbose_name='Название')
    slug = models.SlugField(
        max_length=255,
        db_index=True,
        verbose_name="URL",
        unique=True,
        blank=True
    )
    description = models.TextField(
        max_length=1000,
        verbose_name='Описание',
        blank=True
    )
    year = models.CharField(
        verbose_name='Год',
        max_length=4,
        validators=[RegexValidator(r'19|20\d{2}')]
    )
    rating = models.CharField(
        verbose_name='Рейтинг',
        max_length=4,
        blank=True
    )
    poster_url_preview = models.ImageField(
        upload_to='posters-preview',
        verbose_name='Изображение постера (preview)',
        blank=True
    )
    poster_url = models.ImageField(
        upload_to='posters',
        verbose_name='Изображение постера',
        blank=True
    )
    video = models.FileField(
        upload_to='video',
        verbose_name='Загрузить видео',
        blank=True
    )
    quality = models.CharField(
        max_length=2,
        choices=QUALITY,
        verbose_name='Качество',
        default='HD',
        blank=True
    )
    type = models.CharField(
        max_length=30,
        choices=TYPE,
        verbose_name='Тип',
        blank=True
    )
    film_length = models.CharField(
        max_length=3,
        verbose_name='Продолжительность',
        blank=True
    )
    countries = models.ManyToManyField('Country', verbose_name='Страна', blank=True)
    genres = models.ManyToManyField('Genre', verbose_name='Жанры', blank=True)
    actors = models.ManyToManyField('Actor', verbose_name='Актёр', blank=True)
    directors = models.ManyToManyField('Director', verbose_name='Режисёр', blank=True)
    categories = models.ManyToManyField('Category', verbose_name='Категория', blank=True, default='')
    is_publish = models.BooleanField(default=True, verbose_name='Опубликовано')
    favorites = models.ManyToManyField(User, related_name='favorite', default=None, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('movie', kwargs={'pk': self.pk})

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'
        unique_together = ('title', 'year')


class Reviews(models.Model):
    """ Комментарий """
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE, verbose_name='Фильм', related_name='reviews_movie', blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь', blank=True, null=True)
    text_review = models.TextField(verbose_name='Отзыв',  max_length=200)
    create_data = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=False, verbose_name='Опубликован')

    def __str__(self):
        return f'{self.movie}'

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'


class Genre(models.Model):
    """ Жанр """
    genre = models.CharField(max_length=30, verbose_name='Жанр')
    is_publish = models.BooleanField(default=True, verbose_name='Опубликовано')

    def __str__(self):
        return self.genre

    class Meta:
        verbose_name = 'Жанр'
        verbose_name_plural = 'Жанры'


class Actor(models.Model):
    """ Актер """
    actor = models.CharField(max_length=50, verbose_name='Актер')

    class Meta:
        verbose_name = 'Актер'
        verbose_name_plural = 'Актеры'

    def __str__(self):
        return self.actor


class Director(models.Model):
    """ Режисёр """
    director = models.CharField(max_length=50, unique=True, verbose_name='Режисёр')

    def __str__(self):
        return self.director

    class Meta:
        verbose_name = 'Режисёр'
        verbose_name_plural = 'Режисёры'


class Country(models.Model):
    """ Страна """
    country = models.CharField(max_length=50, verbose_name='Страна', unique=True)

    def __str__(self):
        return self.country

    class Meta:
        verbose_name = 'Страна'
        verbose_name_plural = 'Страны'


class Category(models.Model):
    category = models.CharField(max_length=60, verbose_name='Категория', default='')
    is_publish = models.BooleanField(default=True, verbose_name='Опубликовано')
    poster = models.FileField(
        upload_to='posters-category',
        verbose_name='Загрузить preview',
        blank=True
    )

    def __str__(self):
        return self.category

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class RatingView(models.Model):
    STARS = ((num, f'{num}') for num in range(0, 11))
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    movie = models.ForeignKey('Movie', on_delete=models.CASCADE)
    star = models.PositiveSmallIntegerField(default=0, validators=[
        MinValueValidator(0), MaxValueValidator(10)
    ], choices=STARS)

    def __str__(self):
        return f'{self.movie}'
