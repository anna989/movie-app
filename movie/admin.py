from django.contrib import admin
from .models import *


@admin.register(Movie)
class MovieAdmin(admin.ModelAdmin):
    filter_horizontal = ('actors', 'countries', 'genres', 'directors', 'categories')
    list_display = ['title', 'year', 'type', 'film_length', 'rating', 'poster_url', 'is_publish']
    search_fields = ['title']
    prepopulated_fields = {'slug': ("title",)}


@admin.register(Reviews)
class ReviewsAdmin(admin.ModelAdmin):
    list_display = ['movie', 'user', 'create_data', 'status']
    search_fields = ['user']


@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ['genre', 'is_publish']
    search_fields = ['genre']


@admin.register(Actor)
class ActorAdmin(admin.ModelAdmin):
    search_fields = ['actor']


@admin.register(Director)
class DirectorAdmin(admin.ModelAdmin):
    search_fields = ['director']


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ['country']
    search_fields = ['country']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ['category', 'poster', 'is_publish']
    search_fields = ['category']


@admin.register(RatingView)
class RatingViewAdmin(admin.ModelAdmin):
    list_display = ['star', 'user', 'movie']