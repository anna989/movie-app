from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.views.generic.dates import DateMixin

from .models import *


def active_categories():
    categories_id = Category.objects.values_list('id', flat=True)
    return [i for i in categories_id if Movie.objects.filter(categories=i)]


def index(request):
    genres_id = Genre.objects.values_list('id', flat=True)
    genres_filter = [i for i in genres_id if Movie.objects.filter(genres=i)]
    genres = Genre.objects.filter(pk__in=genres_filter)
    movies = {Category.objects.filter(pk=el)[0]: Movie.objects.filter(categories=el)[:9] for el in active_categories()}

    context = {
        'title': 'Главная страница',
        'genres': genres,
        'movies': movies,
    }

    return render(request, 'index.html', context=context)


def show_categories(request):
    categories = Category.objects.filter(pk__in=active_categories())

    context = {
        'categories': categories
    }

    return render(request, 'full_categories.html', context=context)


def show_category(request, category_id):
    movies = Movie.objects.filter(categories=category_id).all()
    title = Category.objects.get(pk=category_id)

    context = {
        'movies': movies,
        'title': title,
    }
    return render(request, 'category_movies.html', context=context)


def show_genre_movies(request, genre_id):
    movies = Movie.objects.filter(genres=genre_id).all()

    context = {
        'movies': movies,
        'title': Genre.objects.filter(pk=genre_id)[0]
    }
    return render(request, 'genre_movies.html', context=context)


def show_actor_movies(request, actor_id):
    movies = Movie.objects.filter(actors=actor_id).all()

    context = {
        'movies': movies,
        'title': Actor.objects.filter(pk=actor_id)[0]
    }
    return render(request, 'actor_movies.html', context=context)


def show_country_movies(request, country_id):
    movies = Movie.objects.filter(countries=country_id).all()

    context = {
        'movies': movies,
        'title': Country.objects.filter(pk=country_id)[0]
    }
    return render(request, 'country_movies.html', context=context)


def show_movie(request, movie_id):
    movie = get_object_or_404(Movie, pk=movie_id)
    actors = movie.actors.all()
    countries = movie.countries.all()
    genres = movie.genres.all()
    directors = movie.directors.all()

    context = {
        'movie': movie,
        'title': movie.title,
        'actors': actors,
        'countries': countries,
        'genres': genres,
        'directors': directors,
    }

    return render(request, 'movie.html', context=context)


def search(request):
    msg = " ".join(request.GET['q'].split())
    movies = Movie.objects.filter(title__iregex=rf'^{msg}|\s{msg}')

    context = {
        'movies': movies,
    }
    if movies and len(msg) > 1:
        return render(request, 'search.html', context=context)
    elif len(msg) == 0:
        return redirect('/full_categories/')
    else:
        return render(request, 'page_error.html', context=context)

