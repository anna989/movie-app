from django.urls import path
from . import views

app_name = 'movie'
urlpatterns = [
    path('', views.HomeView.as_view(), name='home'),
    path('random-movies/', views.MoviesListView.as_view(), name='random_movies'),
    path('genres/<int:pk>/movies/', views.MoviesGenreView.as_view(), name='movies_genre'),
    path('actors/<int:pk>/movies/', views.MoviesActorView.as_view(), name='movies_actor'),
    path('countries/<int:pk>/movies/', views.MoviesCountryView.as_view(), name='movies_country'),
    path('full-categories/', views.MoviesFullCategoriesView.as_view(), name='full_categories'),
    path('full-categories/<int:pk>/', views.MoviesCategoryView.as_view(), name='movies_category'),
    path('movies/<int:pk>/', views.MovieDetailView.as_view(), name='movie'),
    path('add-review/<int:pk>/', views.AddReviewView.as_view(), name='add_review'),
    path('add-rating/', views.AddRatingView.as_view(), name='add_rating'),
    path('search/', views.search, name='search'),
    path('fav/<int:pk>/', views.favorite_add, name='favorite_add'),
    path('favorites/', views.favorite_list, name='favorite_list'),
]
